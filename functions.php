<?php

function get_home_path_safely(): string {
	if(!function_exists('get_home_path')) {
		require_once(ABSPATH . 'wp-admin/includes/file.php');
	}
	return get_home_path();
}

function wp2smol_preformat_before_conversion($html): string {

	//todo: call a filter, to give other plugins the opportunity to preformat

	return $html;

}

//similar to strip_tags except accepts a list of tags to remove
function wp2smol_remove_tags($html, $tags): string {
	$strtr_array = [];
	foreach($tags as $tag) {
		$strtr_array[strtolower($tag)] = '';
		$strtr_array[strtoupper($tag)] = '';
		$strtr_array[strtolower('</' . substr($tag, 1))] = '';
		$strtr_array[strtoupper('</' . substr($tag, 1))] = '';
	}
	return strtr($html, $strtr_array);
}


function wp2smol_ensure_gemini_directory_exists() {
	global $wp2smol_gemini_root;
	$root = get_home_path_safely();
	if(!is_dir("$root$wp2smol_gemini_root")) {
		mkdir("$root$wp2smol_gemini_root");
	}
	if(!is_dir("$root$wp2smol_gemini_root/gemlog")) {
		mkdir("$root$wp2smol_gemini_root/gemlog");
	}
}

function wp2smol_simplify_path($root, $filepath): string {
	global $wp2smol_gemini_root;
	return strtr($filepath, ["$root$wp2smol_gemini_root/" => '']);
}

function wp2smol_simplify_gemlog_path($root, $filepath): string {
	global $wp2smol_gemini_root;
	return strtr($filepath, ["$root{$wp2smol_gemini_root}//gemlog/" => '']);
}


function wp2smol_gmi_title($filename, $separator = ' - '): string {
	$return_value = '';

	//get date from start of filename
	$parts = explode("-", basename($filename));
	if(count($parts) > 3 && is_numeric($parts[0]) && is_numeric($parts[1]) && is_numeric($parts[2])) {
		$return_value .= "$parts[0]-$parts[1]-$parts[2]";
	}
	//get title from first "# title" line in the file
	$handle = fopen($filename, "r");
	if ($handle) {
		while (($line = fgets($handle)) !== false) {
			if(str_starts_with($line, "# ")) {
				$return_value .= $separator . trim(substr($line, 2));
				break;
			}
		}
		fclose($handle);
	}
	return $return_value;
}

function wp2smol_gmi_title_for_atom($filename, $separator = ' - '): string {

	//get title from first "# title" line in the file
	$handle = fopen($filename, "r");
	if ($handle) {
		while (($line = fgets($handle)) !== false) {
			if(str_starts_with($line, "# ")) {
				return trim(substr($line, 2));
			}
		}
		fclose($handle);
	}
	return basename($filename);
}

function wp2smol_atom_header($host): string {
	$atom_date = wp2smol_atom_date('now');
	return "<?xml version='1.0' encoding='UTF-8'?>
 <feed xmlns=\"http://www.w3.org/2005/Atom\">
	 <id>gemini://$host/gemlog/</id>
 	<title>" . get_bloginfo('name') . "</title>
	<updated>$atom_date</updated>
	<author><name>wp2smol</name></author>
	<link href=\"gemini://$host/gemlog/atom.xml\" rel=\"self\"/>
	<link href=\"gemini://$host/gemlog/\" rel=\"alternate\"/>
	<generator uri=\"http://smol.chorebuster.net\" version=\"0.1\">wp2smol</generator>\n";
}

function wp2smol_atom_date($when): string {
	if(is_integer($when)) { // sometimes it's a timestamp
		$return_value = new DateTime();
		$return_value->setTimestamp($when);
		$return_value->setTimezone(new DateTimeZone('UTC'));
	}
	else {
		$return_value = new DateTime($when, new DateTimeZone('UTC'));  // e.g. '2022-09-18T15:47:47.261110+00:00';
	}
	return $return_value->format(DateTime::ATOM);
}

function wp2smol_atom_item($filename, $title): string {
	$host = $_SERVER['HTTP_HOST'];
	$updated = wp2smol_atom_date(filemtime( $filename));
	$short_filename = basename($filename);
	return "\t<entry>
		<id>gemini://$host/gemlog/$short_filename</id>
		<title>$title</title>
		<updated>$updated</updated>
		<link href=\"gemini://$host/gemlog/$short_filename\" rel=\"alternate\"/>
	</entry>\n";
}
