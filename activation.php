<?php

if ( !defined('ABSPATH') ) { 
    die;
}

class Activation {

    public static function activate() {
    
        //give the administrator role permission to manage this plugin's settings
        $role = get_role('administrator');
        
        if(!empty($role)) {
            $role->add_cap('wp2smol_manage');
        }
    }
}
