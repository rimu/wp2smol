<?php
/**
 * @package WP2Smol
 * @version 1.0
 */
/*
Plugin Name: Wordpress 2 Smol
Plugin URI: http://wordpress.org/plugins/wp2smol/
Description: Creates Gemtext version of your website for automatic publishing to the Small Internet.
Author: Rimu Atkinson
Version: 1.0
Author URI: http://rimu.geek.nz/
License: GPL v2 or later
*/ 

if ( !defined('ABSPATH') ) { 
    die;
}

$wp2smol_gemini_root = 'wp-content/gemini';

require_once "functions.php";


register_activation_hook( __FILE__, function() {
    require_once plugin_dir_path( __FILE__ ) . 'activation.php';
    Activation::activate();
} );


register_uninstall_hook( __FILE__, 'wp2smol_uninstall' );
function wp2smol_uninstall() {
    $role = get_role('administrator');
    if(!empty($role)) {
        $role->remove_cap('wp2smol_manage');
    }
}



// Add a menu for our option page
add_action( 'admin_menu', function() {
        add_options_page( 'WP2Smol Settings', 'WP2Smol', 'wp2smol_manage',
        'wp2smol_plugin', 'wp2smol_settings_page' );
});

function wp2smol_settings_page() {
    global $wp2smol_gemini_root;
    ?>
    <div class="wrap">
        <h2>WP2Smol settings</h2>
        <p>This plugin will save your site as gemtext to <strong><?= get_home_path_safely() ?><?= $wp2smol_gemini_root ?></strong> so set
            your Gemini server to use that as the site root.</p>
        <form action="options.php" method="post">
            <?php 
            settings_fields( 'wp2smol_plugin_options' );
            do_settings_sections( 'wp2smol_plugin' );
            submit_button( 'Save Changes', 'primary' );  
            ?>
        </form>
    </div>
    <?php
}
        
// Register and define the settings fields
add_action('admin_init', 'wp2smol_plugin_admin_init');

function wp2smol_plugin_admin_init(){
    $args = array(
        'type'=> 'string', 
        'sanitize_callback' => 'wp2smol_plugin_validate_options',
        'default' => NULL
    );

    //register our settings
    register_setting( 'wp2smol_plugin_options', 'wp2smol_plugin_options', $args );
    
    //add a settings section
    add_settings_section( 
        'wp2smol_plugin_main', 
        'General settings',
        'wp2smol_plugin_section_text', 
        'wp2smol_plugin' 
    );
    
    //create our settings field for name
    add_settings_field( 
        'pdev_plugin_name', 
        'API Key',
        'wp2smol_plugin_setting_api_key', 
        'wp2smol_plugin', 
        'wp2smol_plugin_main' 
    );

}

// Draw the section header
function wp2smol_plugin_section_text() {

    echo '<p></p>';

}
        
// Display and fill the Name form field
function wp2smol_plugin_setting_api_key() {

    // get option 'text_string' value from the database
    $options = get_option( 'wp2smol_plugin_options' );
    $api_key = $options['api_key'];

    // echo the field
    echo "<input id='api_key' name='wp2smol_plugin_options[api_key]'
        type='text' value='" . esc_attr( $api_key ) . "' size='70' /><br /><small>Get a free API key from <a href='https://rapidapi.com'>RapidAPI</a> to use this plugin.</small>";

}

// Validate user input (we want text and spaces only)
function wp2smol_plugin_validate_options( $input ) {

    $valid = array();
    $valid['api_key'] = preg_replace(
        '/[^a-zA-Z0-9\s]/',
        '',
        $input['api_key'] );

    if($valid['api_key'] !== $input['api_key']) {
        add_settings_error('wp2smol_plugin_options', 'wp2smol_api_key_error', 'Invalid API key, changes not saved.', 'error');
    }

    return $valid;

}


add_action('save_post', 'wp2smol_save_post', 10, 3);

function wp2smol_save_post($post_id, $post, $update) {
    $options = get_option( 'wp2smol_plugin_options' );
    $api_key = $options['api_key'];
    $regenerate = false;
    if(!empty($api_key)) {
	    if($post->post_status == 'publish') {
		    $content = $post->post_content; //todo: check if this needs to be filtered
            $tags = get_the_tags($post->ID);
            if(!empty($tags)) {
                $content .= "\n\n<p>Tags: #" . implode(', #', array_column($tags, 'name')) . "</p>";
            }
		    $title = $post->post_title;
		    $gemtext = wp2smol_convert_to_gemtext($content);
		    $type = $post->post_type;
		    if($type == 'post') {
			    wp2smol_save_post_as_gemtext($post, $gemtext);
			    $regenerate = true;
		    }
            elseif($type == 'page') {
			    wp2smol_save_page_as_gemtext($post->post_name, $title, $gemtext);
			    $regenerate = true;
		    }
	    }
	    elseif($post->post_status == 'trash') {
		    wp2smol_delete_post($post_id, $post);
        }
	    if($regenerate) {
		    wp2smol_regenerate_home();
	    }
    }
}

function wp2smol_convert_to_gemtext($html): string {
    if(empty($html)) {
        return '';
    }
    $options = get_option( 'wp2smol_plugin_options' );
    $api_key = $options['api_key'];

    $endpoint = 'https://html-to-gemtext-or-markdown.p.rapidapi.com/html2gmi/';
    if($_SERVER['HTTP_HOST'] == 'wp2smol') {                //for local debugging
	    $endpoint = 'http://127.0.0.1:8000/html2gmi/';
    }

    $body = [
        'html'  => wp2smol_preformat_before_conversion($html),
        'html2md_options' => [
            'strip' => ['b', 'strong', 'em', 'i'],
            'escape_underscores' => false,
            'escape_asterisks' => false
        ],
        'md2gmi_options' => [
            'plain' => true,
            'links' => 'paragraph'
        ]
    ];

    // allow other plugins to alter parameters passed to the API
    $body = apply_filters('wp2smol_filter_api_params', $body);

    $body = wp_json_encode( $body );

    $options = [
        'body'        => $body,
        'headers'     => [
            'Content-Type' => 'application/json',
            'X-RapidAPI-Host' => 'html-to-gemtext-or-markdown.p.rapidapi.com',
            'X-RapidAPI-Key' => $api_key,
        ],
        'timeout'     => 60,
        'redirection' => 5,
        'blocking'    => true,
        'httpversion' => '1.0',
        'sslverify'   => false,
        'data_format' => 'body',
    ];

    $response = wp_remote_post( $endpoint, $options );
	if ( is_wp_error( $response ) ) {
		$error_message = $response->get_error_message();
		//@todo display error message
        return '';
	}
	else {
	    $response_json = json_decode($response['body']);
		return $response_json->result;
    }
}

function wp2smol_save_post_as_gemtext($post, $content) {
	global $wp2smol_gemini_root;
	$root = get_home_path_safely();
	$filename = $post->post_name;
	$title = $post->post_title;
	$when = $post->post_date;

    wp2smol_ensure_gemini_directory_exists();

    //find the date and throw away the time part, after the space
    $parts = explode(' ', $when);
    $when = $parts[0];

    // if comments are allowed, link to the HTML page where they can fill in the form
	$comment_gemtext = '';
    if($post->comment_status == 'open') {
	    $comment_gemtext .= "\n=> " . $post->guid . "#commentform 📝 Comment on this post\n";
    }

    // if there are comments, render them all as one chunk of HTML and then convert it to gemtext
    if($post->comment_count > 0) {
	    $comments = get_approved_comments($post->ID);
	    if(!empty($comments)) {
		    $comment_html = '<h2>Comments</h2>';
		    foreach ( $comments as $comment ) {
			    $comment_html .= "<h3>{$comment->comment_author}</h3>\n";
			    $comment_content = strtr($comment->comment_content, ["\r\n" => "<br />"]);
			    $comment_html .= "<p>$comment_content</p>";
		    }
		    $comment_gemtext .= wp2smol_convert_to_gemtext($comment_html);
        }
    }

    //link to home page for at the bottom
    $home = "=> index.gmi 🔙 Gemlog\n=> .. 🔝 Home\n\n";

    $final_gemtext = apply_filters('wp2smol_filter_post_gemtext', "# $title\n\n$content\n\n$comment_gemtext\n\n$home");

    //booya
    file_put_contents("$root$wp2smol_gemini_root/gemlog/$when-$filename.gmi", $final_gemtext);
}

//redo post when someone comments on it
add_action('comment_post', 'wp2smol_save_comment', 10, 2);
function wp2smol_save_comment($comment_id, $comment_approved) {
    if($comment_approved == 1) {
	    $comment = get_comment( $comment_id );
	    $post = get_post($comment->comment_post_ID);
	    wp2smol_save_post_as_gemtext($post, $post->post_content);
    }
}

function wp2smol_save_page_as_gemtext($filename, $title, $content) {
	global $wp2smol_gemini_root;
	$root = get_home_path_safely();

	wp2smol_ensure_gemini_directory_exists();

	$final_gemtext = apply_filters('wp2smol_filter_page_gemtext', "# $title\n\n$content");

	file_put_contents("$root$wp2smol_gemini_root/$filename.gmi", $final_gemtext);
}

function wp2smol_regenerate_home() {
	global $wp2smol_gemini_root;
	$host = $_SERVER['HTTP_HOST'];
	$site_name = get_bloginfo('name');
	$site_tagline = html_entity_decode(get_bloginfo('description'));
    $root = get_home_path_safely();
    $dir = "$root$wp2smol_gemini_root/";
	$gemlog_dir = "$dir/gemlog/";

	// regenerate /gemlog/index.gmi
	$filepaths = glob($gemlog_dir . '*.gmi');
	rsort($filepaths, SORT_NATURAL);
	$atom_items = [];

	$gemlogs_added = 0;
	$gemlog_index = "# $site_name - Gemlog\n\n";
	foreach ($filepaths as $filepath) {
		$basic_filepath = wp2smol_simplify_gemlog_path($root, $filepath);
		if($basic_filepath != 'index.gmi') {
			$gemlog_index .= "=> " . $basic_filepath . " " . wp2smol_gmi_title($filepath) . "\n";
			$gemlogs_added++;
			$atom_items[$filepath] = wp2smol_gmi_title_for_atom($filepath);
		}
	}
	$gemlog_index .= "\n=> .. 🔝 Home\n\n";

	//footer
	$gemlog_index .= "\n=> mailto:" . get_bloginfo('admin_email') . " Contact\n";
	$gemlog_index .= "=> " . get_bloginfo('url') . " HTML version \n";
	$gemlog_index .= "=> " . get_bloginfo('rss_url') . " RSS feed (HTTP links)\n";
	$gemlog_index .= "=> atom.xml Atom feed (gemini links)\n";

	if($gemlogs_added == 0) {
		unlink("$gemlog_dir/index.gmi");
	}
	else {
		$final_gemtext = apply_filters('wp2smol_filter_gemlog_index', $gemlog_index);
		file_put_contents($gemlog_dir . 'index.gmi', $final_gemtext);
    }

	// regenerate /atom.xml
    if(count($atom_items)) {
        $atom = wp2smol_atom_header($host);
        foreach($atom_items as $filename => $title) {
            $atom .= wp2smol_atom_item($filename, $title, 'wp2smol');
        }
        $atom .= "\n</feed>\n";
        file_put_contents($gemlog_dir . 'atom.xml', $atom);
    }
    else {
        unlink("$gemlog_dir/atom.xml");
    }

	// regenerate /index.gmi
    $site_index = "# $site_name\n\n";
	$site_index .= "$site_tagline\n\n";

	if($gemlogs_added) {
	    $site_index .= "=> gemlog/ Gemlog\n";
    }
	$filepaths = glob($dir . '*.gmi');
	sort($filepaths, SORT_NATURAL);

	foreach ($filepaths as $filepath) {
		$basic_filepath = wp2smol_simplify_path($root, $filepath);
		if($basic_filepath != 'index.gmi') {
			$site_index .= "=> " . $basic_filepath . " " . wp2smol_gmi_title($filepath, ' ') . "\n";
		}
	}

	//footer
    $site_index .= "\n=> mailto:" . get_bloginfo('admin_email') . " Contact\n";
	$site_index .= "=> " . get_bloginfo('url') . " HTML version \n";
	$site_index .= "=> " . get_bloginfo('rss_url') . " RSS feed (HTTP links)\n";
	$site_index .= "=> gemlog/atom.xml Atom feed (gemini links)\n";

	$final_gemtext = apply_filters('wp2smol_filter_site_index', $site_index);

	file_put_contents($dir . 'index.gmi', $final_gemtext);

}


add_action('delete_post', 'wp2smol_delete_post', 10, 2);

function wp2smol_delete_post($post_id, $post) {
	global $wp2smol_gemini_root;
	$root = get_home_path_safely();
	$type = $post->post_type;
	$post_name = strtr($post->post_name, ['__trashed' => '']);
	$regenerate = false;
	if($type == 'post') {
		$parts = explode(' ', $post->post_date);
		$when = $parts[0];
        if(file_exists("$root$wp2smol_gemini_root/gemlog/$when-$post_name.gmi")) {
            unlink("$root$wp2smol_gemini_root/gemlog/$when-$post_name.gmi");
        }
		$regenerate = true;
    }
	elseif($type == 'page') {
		if(file_exists("$root$wp2smol_gemini_root/$post_name.gmi")) {
			unlink("$root$wp2smol_gemini_root/$post_name.gmi");
		}
		$regenerate = true;
	}
	if($regenerate) {
		wp2smol_regenerate_home();
	}
}


